//
//  ViewController.swift
//  FLMountainViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var mountainScrollView: UIScrollView!
    
    var mainMountainView: MountainView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createMainMountain()
        createMiniMountain()
        self.mountainScrollView.delegate = self
    }

    func createMainMountain() {
        let width = 360
        let height = 370
        let y = 768 - height
        let x = (1024/2) - (width/2)
        
        mainMountainView = MountainView(
            name: "mountain-main",
            maxHeight: 550,
            frame: CGRect(x: x, y: y, width: width, height: height),
            valueChanged:{(value: CGFloat, name:String) in
                print("\(name): \(value)")
            }
        )
        
        self.mountainScrollView.addSubview(mainMountainView)
    }
    
    func createMiniMountain() {
        let width = 145
        let height = 140
        let y = 768 - height
        
        for index in 0...9 {
            let miniMountain: MountainView = MountainView(
                name: "mountain-\(index)",
                maxHeight: 205,
                frame: CGRect(x: index * width, y: y, width: width,height: height),
                valueChanged:{(value: CGFloat, name:String) in
                    print("\(name): \(value)")
                }
            )
            self.mountainScrollView.addSubview(miniMountain)
        }
        self.mountainScrollView.contentSize = CGSize(width: 10 * width, height: 768)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let width: CGFloat = 360
        let y = mainMountainView.frame.origin.y
        let x = (1024/2) - (width/2) + scrollView.contentOffset.x
        mainMountainView.frame.origin = CGPoint(x: x, y: y)
    }
}
