//
//  MountainView.swift
//  FLMountainViewController
//
//  Created by Peerasak Unsakon on 11/6/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

typealias MountainValueDidChanged = (_ value: CGFloat, _ name: String ) -> ()

class MountainView: UIView {
    var translation: CGPoint!
    var startPosition: CGPoint!
    var originalHeight: CGFloat = 0
    var maxHeight: CGFloat!
    var difference: CGFloat!
    var mountainValue: CGFloat = 0
    var name: String = "mountain-main"
    
    var valueDidChanged: MountainValueDidChanged?
    
    var mountainImageView: UIImageView!
    var mountainLabel: UILabel!
    
    init(name: String, maxHeight: CGFloat, frame: CGRect, valueChanged: @escaping MountainValueDidChanged) {
        super.init(frame: frame)
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(didPan(sender:)))
        self.addGestureRecognizer(gesture)
        self.isUserInteractionEnabled = true
    
        self.name = name
        self.setMountainImage()
        self.maxHeight = maxHeight
        self.originalHeight = self.frame.height
        self.createMountainLabel()
        self.valueDidChanged = valueChanged
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)

    }
    
    func createMountainLabel() {
        let labelWidth = self.frame.width * 0.7
        mountainLabel = UILabel()
        mountainLabel.frame.origin = CGPoint(x: (self.frame.width - labelWidth)/2, y: -40)
        mountainLabel.frame.size = CGSize(width: labelWidth, height: 35)
        mountainLabel.backgroundColor = .black
        mountainLabel.textColor = .white
        mountainLabel.textAlignment = .center
        mountainLabel.text = "0"
        self.addSubview(mountainLabel)
        self.setMountainValue()
    }
    
    func setMountainImage() {
        if let image = UIImage(named: name) {
            mountainImageView = UIImageView(image: image)
            mountainImageView.frame.size = self.frame.size
            self.addSubview(mountainImageView)
        }
    }
    
    func setMountainValue() {
        let max = self.maxHeight - self.originalHeight
        let difference = self.frame.height - self.originalHeight
        mountainValue = round((difference / max) * 100)
        self.mountainLabel.text = String(format: "%.0f", mountainValue)
    }
    
    @objc func didPan(sender: UIPanGestureRecognizer) {
        
        guard let mountainView = sender.view else { return }

        if sender.state == .began {
             startPosition = sender.location(in: mountainView)
        }
        
        if sender.state == .began || sender.state == .changed {
            translation = sender.translation(in: mountainView)
            let newY = self.frame.origin.y + translation.y
            
            if let superview = self.superview {
                let newHeight = superview.frame.size.height - newY
                if newHeight >= originalHeight && newHeight <= maxHeight {
                    self.frame = CGRect(x: self.frame.origin.x, y: newY, width: self.frame.size.width, height: newHeight)
                    self.setMountainValue()
                    if let block = valueDidChanged {
                        
                        block(self.mountainValue, name)
                    }
                }
            }
        }
        
        if sender.state == .ended {
            
        }
        
        sender.setTranslation(CGPoint(x: 0.0, y: 0.0), in: mountainView)
        mountainImageView.frame.size = self.frame.size
    }
}
